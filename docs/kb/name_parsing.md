# Comment Zotero analyse-t-il le contenu des champs de nom ?

*Consulter cette page dans la documentation officielle de Zotero : [How does Zotero parse things in the name fields?](https://www.zotero.org/support/kb/name_parsing) - dernière mise à jour de la traduction : 2023-02-09*
{ data-search-exclude }

Il y a en fait trois parties dans l'histoire des noms dans Zotero ("créateurs", dans le jargon technique) :

1. les types de créateurs,
2. le mode de champ, et
3. l'analyse des parties du nom.

Chacun de ces sujets est abordé ci-dessous. Les deux premiers sont très simples.

## Types de créateurs

Chaque champ de nom a une étiquette à sa gauche, qui est en fait un bouton. En cliquant dessus, vous ouvrez une liste des *types de créateurs* possibles pour le type de document en cours. Vous pouvez changer le type de chaque créateur individuellement, en cliquant sur son étiquette et en sélectionnant le type à appliquer dans la liste.

## Mode de champ

Il y a une petite icône carrée à droite de chaque nom, juste avant les boutons **(+)** et **(-)** utilisés pour ajouter et supprimer des créateurs. En cliquant sur l'icône carrée, le nom bascule entre le mode *champ unique* et le mode *deux champs*.

* Dans le mode "champ unique", le contenu du champ n'est pas analysé lors de la génération des citations [^1]. Ce mode est généralement utilisé pour les noms d'institutions.
* Dans le mode "deux champs", le champ est analysé en parties (encore) plus petites lors de la génération des citations. Le mode à deux champs doit normalement être utilisé pour les noms de personnes. Cela inclut les noms asiatiques ! Le processeur CSL de Zotero peut mettre en forme correctement les noms dans une variété de langues [^2], et dans tous les styles bibliographiques. Cette flexibilité nécessite toutefois que les données soient correctement saisies. Ce n'est pas une bonne pratique de "forcer" une forme particulière en sélectionnant inutilement le mode champ unique.

## Analyse des parties du nom

Dans le mode deux champs uniquement, les noms de personnes sont décomposés en cinq parties distinctes pour des raisons de mise en forme. Les voici, avec une brève explication de chacune d'elles.

* *Nom de famille* : Le nom de famille ou de clan d'un individu est le "nom de famille" primaire dans Zotero [^3] : 
  * Le nom de famille de "Sam Spade" est "Spade".
  * Le nom de famille de "Jeremy Atticus Finch" est "Finch".
  * Le nom de famille de "Kuruma Torajirō" est "Kuruma" (notez que la partie du nom de famille de ce personnage japonais est écrite en premier).
* *Prénom* : Il s'agit du ou des noms "individuels" d'une personne :
  * Le prénom de "Sam Spade" est "Sam".
  * Le prénom de "Jeremy Atticus Finch" est "Jeremy Atticus".
  * Le prénom de "Kuruma Torajirō" est "Torajirō" (le protagoniste principal de la série japonaise "Tora-san").
* *Particule rejetée* : Les particules rejetées, une caractéristique de certains noms européens, sont des éléments descriptifs qui sont placés entre le prénom et le nom de famille lorsqu'ils sont écrits dans l'ordre "normal". Une particule rejetée n'est jamais placée avec le nom de famille lorsqu'il est écrit dans l'ordre "trié".
  * Dans "Ludwig van Beethoven", "van" est une particule rejetée.
  * Dans "Jean de La Fontaine", "de" est une particule rejetée.
 * Particule non rejetée :
 * Articulaire

***

 [^1]: Dans la variante Juris-M (anciennement Zotero multilingue/MLZ) de Zotero officiel, les noms de champs uniques sont analysés en sous-unités en divisant le champ sur les caractères barres verticales ("|"). Dans la version officielle de Zotero, le champ est rendu exactement tel qu'il est saisi.

 [^2]: Les noms chinois et japonais s'affichent correctement dans la version officielle de Zotero. Les noms dans certaines langues (le khmer et le myanmar en sont deux exemples) ne sont pas encore traités correctement par la version officielle de Zotero ; les utilisateurs ayant des exigences particulières peuvent souhaiter explorer Juris-M, qui est capable d'appliquer des règles précises de mise en forme des noms dans tous les domaines linguistiques.

 [^3]: Dans certains autres pays, les individus n'ont pas de nom de famille ou de clan, mais seulement des prénoms. Les conventions de mise en forme dans ces pays varient. Au Myanmar et au Cambodge, l'ensemble des noms est toujours écrit dans les contextes formels (y compris la citation). En Mongolie, il est d'usage de traiter le patronyme nu de la même manière qu'un nom de "famille". Lorsque des noms présentant des exigences particulières doivent être traités fréquemment, Juris-M peut être intéressant.
