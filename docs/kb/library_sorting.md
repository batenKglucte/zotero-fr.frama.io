# Puis-je changer la façon dont les documents sont triés dans ma bibliothèque ?

*Consulter cette page dans la documentation officielle de Zotero : [Can I change the way items are sorted in my library?](https://www.zotero.org/support/kb/library_sorting) - dernière mise à jour de la traduction : 2022-10-20*
{ data-search-exclude }

Oui, consultez la page sur le [tri](../sorting.md).
