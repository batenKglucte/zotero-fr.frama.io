# Zotero prend-il en charge les caractères non occidentaux ?

*Consulter cette page dans la documentation officielle de Zotero : [Does Zotero support non-Western characters?](https://www.zotero.org/support/kb/non-western_characters) - dernière mise à jour de la traduction : 2023-03-30*
{ data-search-exclude }

Oui, Zotero est compatible avec Unicode et peut traiter des caractères non occidentaux.