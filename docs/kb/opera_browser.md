# Comment puis-je utiliser Zotero avec le navigateur Opera?

*Consulter cette page dans la documentation officielle de Zotero : [How can I use Zotero with the Opera browser?](https://www.zotero.org/support/kb/opera_browser) - dernière mise à jour de la traduction : 2023-04-06*
{ data-search-exclude }

Pour utiliser Zotero avec le navigateur Opera, installez tout l'abord l'extension d'Opera [Install Chrome Extensions](https://addons.opera.com/en/extensions/details/install-chrome-extensions/), qui permet à Opera d'exécuter les extensions Chrome. Installez ensuite le [connecteur Zotero pour Chrome](https://chrome.google.com/webstore/detail/ekhagklcjbdpajgpjgmbionohlpdbjgc). Vous pouvez alors utiliser normalement le connecteur Zotero.
