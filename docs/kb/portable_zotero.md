# Puis-je quand même utiliser Zotero si je ne peux pas installer de programmes sur mon ordinateur ?

*Consulter cette page dans la documentation officielle de Zotero : [Can I still use Zotero if I can't install programs on my computer?](https://www.zotero.org/support/kb/portable_zotero) - dernière mise à jour de la traduction : 2023-03-30*
{ data-search-exclude }

Si vous ne pouvez pas installer de programmes sur un ordinateur, vous pouvez toujours utiliser Zotero à l'aide d'une version portable du logiciel sur un disque externe (par exemple une clé USB).

Une version portable de Zotero développée par la communauté est disponible [ici](https://github.com/pedrom34/ZoteroPortable). Notez que cette application portable n'est pas officiellement distribuée par Zotero, et que les développeurs de Zotero ne peuvent fournir qu'un support limité.

Une discussion supplémentaire sur l'application portable Zotero 5.0 est disponible [ici](https://www.zotero.org/forum/discussion/64050/5-0-portable-zotero/p1).