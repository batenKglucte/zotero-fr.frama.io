# Les références apparaissent dans la mauvaise police dans Word/LibreOffice

*Consulter cette page dans la documentation officielle de Zotero : [References appear in the wrong font in Word/LibreOffice](https://www.zotero.org/support/kb/word_default_font) - dernière mise à jour de la traduction : 2023-02-24*
{ data-search-exclude }

Les modules de traitement de texte appliquent le style "Default" (LibreOffice/OpenOffice) ou "Normal"/"Standard" (Word) aux citations générées et au paragraphe dans lequel elles sont insérées. La bibliographie est rendue dans un style différent - "Bibliographie" (Word) ou "Référence1" (LibreOffice/OpenOffice). Par conséquent, l'insertion d'une citation avec Zotero peut supprimer la mise en forme (les retraits, l'interligne, la taille de la police, etc.) d'un paragraphe entier et la citation peut apparaître dans une police ou un format non souhaité. Vous pouvez corriger la mise en forme en ajustant le style "Default/Normal/Standard" (pour les citations) et le style "Bibliography/Reference1" (pour la bibliographie) dans votre traitement de texte.

## Solutions

### Word 2002 et 2003

Si votre barre d'outils de styles et de mise en forme est ouverte (il s'agit d'un menu déroulant avec une liste d'étiquettes telles que "Normal", "Corps du texte", "Titre 1"), vous pouvez simplement faire un clic droit sur le style "Normal" ou "Bibliographie" et le mettre en forme. Sinon, sélectionnez "Styles et mise en forme" dans la barre d'outils de mise en forme ou dans le menu Format. Voir [les instructions officielles de Microsoft : "Personnaliser ou créer des styles"](https://support.microsoft.com/fr-fr/office/personnaliser-ou-cr%C3%A9er-des-styles-d38d6e47-f6fc-48eb-a607-1eb120dec563).

### Word pour Windows 2007 et versions ultérieures

Placez votre curseur dans une citation ou une bibliographie Zotero. Ensuite, cliquez sur le bouton déroulant dans le coin inférieur droit du sélecteur rapide de style de l'onglet "Accueil" et choisissez "Modifier le style". Apportez les modifications nécessaires à la police et à la mise en forme des paragraphes pour les styles "Normal" ou "Bibliographie" et cliquez sur "OK". Vous pouvez également modifier la mise en en forme de citations individuelles en utilisant les options des groupes "Police" et "Paragraphe" de l'onglet "Accueil". Voir [un guide avec des captures d'écran (en anglais)](http://www.lostintechnology.com/how-to/how-to-change-the-default-settings-in-microsoft-word-2007).

### Word pour Mac 2008 et ultérieur

Placez votre curseur dans une citation ou une bibliographie Zotero. Cliquez ensuite sur le bouton "Volet des styles" à l'extrémité de l'onglet "Accueil". Cliquez sur le titre "Style actuel" en haut du volet et choisissez "Modifier le style". Apportez les modifications nécessaires à la police et à la mise en forme des paragraphes pour les styles "Normal" ou "Bibliographie" et cliquez sur "OK". Vous pouvez également modifier la mise en forme des citations individuelles en utilisant les options "Police" et "Paragraphe" du menu "Format".

### LibreOffice/OpenOffice

Dans LibreOffice, ouvrez le gestionnaire de styles dans "Format" -> "Styles et formatage" ou en appuyant sur F11 (F12 dans OpenOffice). Faites un clic droit sur "Défaut" ou "Bibliographie", sélectionnez "Modifier" et apportez les modifications souhaitées à ce style.
